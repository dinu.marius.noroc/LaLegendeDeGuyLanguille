package fr.ece.java.eel;

import fr.ece.java.eel.data.*;
import fr.ece.java.eel.exception.AssetLoadingException;
import fr.ece.java.eel.exception.MapNotFoundException;
import fr.ece.java.eel.graphics.GameAssets;
import fr.ece.java.eel.map.Map;
import fr.ece.java.eel.map.MapReader;
import fr.ece.java.eel.net.GameServerState;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public class GameServerController implements TurnManagerInterface {
    // the game grid and entities
    private Map map;

    // player list
    private java.util.Map<Turn, Player> players;

    // current turn and next turn, allows us to check if the turn has changed
    private Turn currentTurn;
    private Turn nextTurn;

    // the list of server updates (e.g. a player moved, the turn changed, etc.) we need to send to the client
    private Queue<ServerUpdate> pendingUpdates;

    // allows us to determine if we need to send a ServerUpdate
    private GameServerState gameState;
    private GameServerState lastState;

    private GameAssets assets;

    // server init
    public GameServerController() {
        // init collections and objects
        pendingUpdates = new LinkedBlockingQueue<>();
        players = new LinkedHashMap<>(); // linked hash map : preserve insertion order
        gameState = lastState = GameServerState.WAITING_PLAYERS;
        currentTurn = Turn.WAIT;

        // load level
        MapReader mr = new MapReader();
        try {
            map = mr.loadLevel("level1");
        } catch (MapNotFoundException e) {
            System.err.println("Erreur : impossible de charger le niveau, les ressources sont elles correctement" +
                    "enregistrées ?");
            return;
        }

        // load assets
        this.assets = new GameAssets();
        try {
            assets.serverLoad();
            map.load(assets);
        } catch (AssetLoadingException e) {
            System.err.println("Impossible de charger le Tileset.");
            return;
        }

        // create "empty" players that clients will connect on
        players.put(Turn.PLAYER_1, new Player(Turn.PLAYER_1, this).spawnPoint(map));
        players.put(Turn.PLAYER_2, new Player(Turn.PLAYER_2, this).spawnPoint(map));
        players.put(Turn.PLAYER_3, new Player(Turn.PLAYER_3, this).spawnPoint(map));
        players.put(Turn.PLAYER_4, new Player(Turn.PLAYER_4, this).spawnPoint(map));

        // add them to the map
        players.forEach((turn, player) -> map.addEntity(player));
    }

    /**
     * Adds a player change to the pending update queue
     */
    public void playerChanged(Player player) {
        pendingUpdates.add(new PlayerUpdate(player));
    }

    /**
     * This function is called by the server life thread to update the game logic.
     */
    public void updateLogic() {
        // we want to change turns only once per update
        nextTurn = currentTurn;

        // should handle turns like AI
        specialTurns();

        map.getEntities().forEach((e) -> {
            // update the entities
            e.update();
            // check if the player have moved or changed in any way
            if (e.hasChanged()) {
                // if so, send a player update to the clients
                if (e instanceof Player) {
                    playerChanged((Player) e);
                }
                e.resetHasChanged();
            }
        });

        // if the turn or game state has changed
        if (currentTurn != nextTurn || !lastState.equals(gameState)) {
            pendingUpdates.add(new GameStatusUpdate(this));
        }

        currentTurn = nextTurn;
        lastState = gameState;
    }

    public java.util.Map<Turn, Player> getPlayers() {
        return players;
    }

    public Map getMap() {
        return map;
    }

    /**
     * Get the next available player slot
     *
     * @return Player|null
     */
    public Player getPlayerSlot() {
        Player availablePlayer = null;

        for (Player player : players.values()) {
            if (player.isAvailable()) {
                availablePlayer = player;
                break;
            }
        }

        return availablePlayer;
    }

    public void specialTurns() {
        if (currentTurn.equals(Turn.WAIT) || currentTurn.equals(Turn.AI)) {
            changeToNextTurn();
        }
    }

    private void changeToNextTurn() {
        if (players.containsKey(currentTurn)) {
            players.get(currentTurn).notifyTurnEnd();
        }
        nextTurn = currentTurn.nextTurn();
        if (players.containsKey(nextTurn)) {
            players.get(nextTurn).notifyTurn();
        }
    }

    // called by a player when it's turn is done
    @Override
    public void playerTurnDone(Player player) {
        if (player.getTurnNumber().equals(currentTurn)) {
            changeToNextTurn();
        }
    }

    @Override
    public Turn getCurrentTurn() {
        return currentTurn;
    }

    public void debug() {
//        map.renderAscii();
//        System.out.println("current turn " + currentTurn);
//        System.out.println(players);
    }

    public GameServerState getGameState() {
        return gameState;
    }

    public Queue<ServerUpdate> getPendingUpdates() {
        return pendingUpdates;
    }

    public Turn getNextTurn() {
        return nextTurn;
    }

    public GameServerState getNextGameState() {
        return gameState;
    }
}
