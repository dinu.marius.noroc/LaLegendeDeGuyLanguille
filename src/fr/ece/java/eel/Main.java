package fr.ece.java.eel;


import fr.ece.java.eel.client.App;
import fr.ece.java.eel.net.Server;

import java.io.IOException;

/**
 * Main class
 * Starts the game client or the game server based on the argument
 *
 * Arguments for server:
 * 1 -> "--server"
 * 2 -> port number
 *
 * Example: java -jar gui.jar server 1234
 *
 * Argument for client:
 * --host=host to connect to (defaults to localhost)
 * --port=port number (defaults to 6664)
 *
 * Example: java -jar guy.jar --host=localhost --port=1234
 */
public class Main {
    public static void main(String[] args) {
        // argument "server" specified
        if (args.length > 0 && args[0].equals("--server")) {
            int port = (args.length > 1) ? Integer.parseInt(args[1]) : Server.DEFAULT_PORT;
            Server server = null;
            try {
                server = new Server(port);
            } catch (Exception e) {
                System.err.println("Impossible de lancer le serveur sur le port " + port);
                return;
            }
            try {
                server.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }
        // client launch specified
        App.main(args);
    }
}
