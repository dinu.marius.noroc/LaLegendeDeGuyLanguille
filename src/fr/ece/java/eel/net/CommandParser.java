package fr.ece.java.eel.net;

import fr.ece.java.eel.action.Action;
import fr.ece.java.eel.exception.MaxQueueSizeException;

/**
 * This class parses client commands like move, attack.
 */
public class CommandParser {
    private PlayerThread playerThread;

    public final String ERROR_WRONG_USAGE = "error: wrong usage of command";
    public final String ERROR_MAX_MOVES = "error: max moves reached";
    public final String OK = "ok";

    public CommandParser(PlayerThread thread) {
        this.playerThread = thread;
    }

    /**
     * Reads the command and does the thing!
     * e.g. => "move right" will add a move right action to the Player action queue.
     * @param data the command
     */
    public void parse(String data) {
        String[] command = data.split(" ");

        switch (command[0]) {
            case "move":
                if (command.length < 2) {
                    playerThread.sendLine(ERROR_WRONG_USAGE);
                    break;
                }

                Action action = moveAction(command[1]);
                if (action == null) {
                    playerThread.sendLine(ERROR_WRONG_USAGE);
                    break;
                }
                try {
                    playerThread.getPlayer().queueMove(action);
                } catch (MaxQueueSizeException e) {
                    playerThread.sendLine(ERROR_MAX_MOVES);
                    break;
                }
                playerThread.sendLine(OK);
                break;
            case "attack": // not implemented...
                try {
                    playerThread.getPlayer().queueMove(Action.ATTACK);
                } catch (MaxQueueSizeException e) {
                    playerThread.sendLine(ERROR_MAX_MOVES);
                    break;
                }
                System.err.println("Attack !");
                playerThread.sendLine(OK);
                break;
            case "done":
                // a player wants to end it's turn
                playerThread.getPlayer().skipTurn();
                break;
            case "status":
                playerThread.getProtocol().sendStatus(playerThread.getServer().getStatus());
                break;
            case "map":
                playerThread.getProtocol().sendMap(playerThread.getServer().getGameController().getMap());
                break;
            case "quit":
                System.err.println("Quit");
                playerThread.sendLine(OK);
                playerThread.close();
                break;
        }
    }

    /**
     * Determines the move action based on a string (e.g. "left" => Action.LEFT)
     * @param direction left, right, up or down
     * @return a direction Action
     */
    private Action moveAction(String direction) {
        Action action = null;
        switch (direction) {
            case "left":
                action = Action.LEFT;
                break;
            case "up":
                action = Action.UP;
                break;
            case "right":
                action = Action.RIGHT;
                break;
            case "down":
                action = Action.DOWN;
                break;
        }
        return action;
    }
}
