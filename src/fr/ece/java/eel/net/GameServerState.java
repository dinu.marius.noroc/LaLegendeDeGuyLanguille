package fr.ece.java.eel.net;

public enum GameServerState {
    WAITING_PLAYERS,
    IN_GAME,
    GAME_OVER
}
