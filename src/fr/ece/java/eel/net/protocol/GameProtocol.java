package fr.ece.java.eel.net.protocol;

import fr.ece.java.eel.action.Action;
import fr.ece.java.eel.client.GameClient;
import fr.ece.java.eel.data.Player;
import fr.ece.java.eel.data.GameStatusUpdate;
import fr.ece.java.eel.map.Map;
import fr.ece.java.eel.data.PlayerUpdate;

import java.io.*;
import java.util.Base64;

public class GameProtocol {
    private PrintWriter out;
    private BufferedReader in;

    private final String FLAG_OBJECT = "<<<OBJECT";

    public GameProtocol(PrintWriter out, BufferedReader in) {
        this.out = out;
        this.in = in;
    }

    public String askName() throws IOException {
        out.println("Player name?");
        String name = in.readLine();
        if (name == null) {
            return null;
        } else if (name.length() > 0) {
            return name.trim();
        } else {
            return "Player";
        }
    }

    public void sendName(String name) throws IOException {
        out.println(name);
    }

    public void sendPlayerInfo(Player player) throws IOException {
        sendObject(player.getTurnNumber());
    }

    public void sendStatus(GameStatusUpdate status) {
        sendObject(status);
    }

    public void sendMap(Map map) {
        sendObject(map);
    }

    // To send serialized data over a socket, we decided to serialize our object to an ObjectOutputStream, then
    // encode the result into a base64 string we can safely send into the socket without risking encoding problems or
    // even new lines.
    public void sendObject(Object object) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            oos.close();
            out.println(FLAG_OBJECT); // print <<<OBJECT
            out.println(Base64.getEncoder().encodeToString(baos.toByteArray())); // print the base64 object
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Object readObject() {
        try {
            String serializedObject = in.readLine();
            // base64 string -> byte array -> deserialized object
            ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(
                    Base64.getDecoder().decode(serializedObject.getBytes())));
            Object o = ois.readObject();
            ois.close();
            return o;
        } catch (IOException e) {
            // bad object received, ignoring
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void sendCommand(Command command) {
        out.println(command.toString().toLowerCase());
    }

    public Object checkReceivedLine(String line) {
        if (line.equals(FLAG_OBJECT)) {
            return readObject();
        }
        return null;
    }

    public void handlePlayerUpdate(GameClient game, PlayerUpdate update) {
        if (game != null) {
            game.handlePlayerUpdate(update);
        }
    }

    public void sendPlayerMove(Action action) {
        out.println(Command.MOVE.toString().toLowerCase() + " " + action.toString().toLowerCase());
        out.flush();
    }
}
