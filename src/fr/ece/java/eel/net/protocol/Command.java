package fr.ece.java.eel.net.protocol;

public enum Command {
    NAME,
    MAP,
    STATUS,
    MOVE,
    DONE, // turn end
}

