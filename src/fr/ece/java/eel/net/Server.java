package fr.ece.java.eel.net;

import fr.ece.java.eel.GameServerController;
import fr.ece.java.eel.data.GameStatusUpdate;
import fr.ece.java.eel.data.ServerUpdate;
import fr.ece.java.eel.exception.ServerStartException;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.*;

/**
 * Serveur de jeu
 */
public class Server {
    /**
     * default port to listen on
     */
    public static final int DEFAULT_PORT = 6664;

    /**
     * tick delay in ms
     */
    public static final int TICK_DELAY = 250;

    /**
     * current port
     */
    private int port;

    /**
     * Server Socket we are listening on
     */
    private ServerSocket ss;

    /**
     * The list of connected clients
     */
    private List<PlayerThread> playerThreads;

    /**
     * The life loop timer
     */
    private Timer lifeTimer;

    /**
     * The game controller
     */
    private GameServerController gameController;

    /**
     * Opens a socket on the specified port
     */
    public Server(int port) throws Exception {
        playerThreads = new ArrayList<>();
        this.port = port;

        try {
            ss = new ServerSocket(port);
        } catch (IOException e) {
            throw new ServerStartException();
        }
    }

    /**
     * Starts listening for new connections and initialize the Game Controller
     */
    public void start() throws IOException {
        // 1. Initialize a game controller: for game logic
        gameController = new GameServerController();
        lifeTimer = new Timer(true);

        // Game life loop : this will call the game logic update every TICK_DELAY (e.g. 250ms)
        lifeTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                gameController.updateLogic();
//                gameController.debug();

                // at every server update, we check if the entities were changed, if so, we send updates to all
                // connected players
                ServerUpdate update;
                while ((update = gameController.getPendingUpdates().poll()) != null) {
                    for (PlayerThread pt : playerThreads) {
                        pt.getProtocol().sendObject(update);
                    }
                }
            }
        }, 0, TICK_DELAY);

        System.out.println("Le serveur écoute sur le port " + port + ".");

        // listen for new connections
        int playerCount;
        while (true) {
            playerCount = playerThreads.size() + 1;
            if (playerCount > 4) {
                // server is full, we drop the connection but keep listening (in case the server empties)
                ss.close();
                continue;
            }
            PlayerThread playerThread = new PlayerThread(this, ss.accept());
            new Thread(playerThread).start();
            playerThreads.add(playerThread);
        }
    }

    public void removePlayerThread(PlayerThread playerThread) {
        playerThreads.remove(playerThread);
    }

    public GameStatusUpdate getStatus() {
        if (gameController != null) {
            return new GameStatusUpdate(gameController);
        }
        return new GameStatusUpdate();
    }

    public GameServerController getGameController() {
        return gameController;
    }
}
