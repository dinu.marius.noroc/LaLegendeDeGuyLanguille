package fr.ece.java.eel.net;

import fr.ece.java.eel.data.Player;
import fr.ece.java.eel.net.protocol.GameProtocol;

import java.io.*;
import java.net.Socket;

public class PlayerThread implements Runnable {
    // The connection socket to the client
    private Socket socket;

    // Streams for writing/reading into the connection
    private PrintWriter stringOutputWriter;
    private BufferedReader in;

    // Server instance
    private Server server;

    // Player instance represented by this thread (assigned in run())
    private Player player;

    // Read and understand string commands like "attack", "move"
    private CommandParser parser;

    // Class that basically stores the ways to communicate with the client (e.g. protocol for serializing objects, etc.)
    private GameProtocol protocol;

    private int playerNumber;

    public PlayerThread(Server server, Socket socket) {
        this.socket = socket;
        this.server = server;
        parser = new CommandParser(this);
        System.out.println("Client connected: " + socket.toString());
    }

    // a client just connected, and this thread was started
    public void run() {
        try {
            // inits
            stringOutputWriter = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.protocol = new GameProtocol(stringOutputWriter, in);

            // get a Player instance for the client
            player = getServer().getGameController().getPlayerSlot();
            if (player == null) {
                // couldn't get an available player slot : the server is full, we close the connection
                close();
            }

            // now let's give a name to our Player
            String name = protocol.askName();

            // now the Player has a name, we will set the Player connection thread (this) and name
            player.assignThread(this, name);

            // tell the client who he is (by sending him it's turn number)
            protocol.sendPlayerInfo(player);

            // announce the player to the other players
            getServer().getGameController().playerChanged(player);

            // while there is incoming data to read
            while (!socket.isClosed()) {
                String inputData = in.readLine();
                if (inputData == null) {
                    // data null : client has closed connection
                    close();
                    return;
                }
                // parse input command
                parser.parse(inputData);
            }
        } catch (IOException e) {
            // in case of connection error
            System.err.println("Connection error");
            e.printStackTrace();
        }
    }

    public void sendLine(String message) {
        stringOutputWriter.println(message);
        stringOutputWriter.flush();
    }

    public void close() {
        if (!socket.isClosed()) {
            sendLine("Goodbye");
            try {
                socket.close();
            } catch (IOException e) {
                System.err.println("Close error");
                e.printStackTrace();
            }
        }
        // client disconnected, we remove the player thread and reset the Player.thread (so the player gets marked as
        // "offline", thus liberating a slot and allowing for a new future connection to that player
        server.removePlayerThread(this);
        player.removeThread();

        // announce the player loss to the other players
        getServer().getGameController().playerChanged(player);
    }

    @Override
    public String toString() {
        return "PlayerThread: player=" + (player == null ? "NULL" : player);
    }

    public Player getPlayer() {
        return player;
    }

    public Server getServer() {
        return server;
    }

    public GameProtocol getProtocol() {
        return protocol;
    }
}
