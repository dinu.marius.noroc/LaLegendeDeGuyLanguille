package fr.ece.java.eel.graphics;

import fr.ece.java.eel.exception.TilesetNotFoundException;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.io.*;
import java.util.*;

public class Tileset {
    // the source tileset image
    Image sourceImage;

    // individual tile height/width
    int tileWidth;
    int tileHeight;

    // list of tiles indexes we can't walk on
    List<Integer> nonWalkable;

    int cols; // number of tile columns in the tileset

    // default constructor
    public Tileset() throws TilesetNotFoundException {
        this("tileset", true);
    }

    public Tileset(boolean loadImage) throws TilesetNotFoundException {
        this("tileset", loadImage);
    }

    /**
     * Inits and loads the tileset!
     *
     * @param tilesetName the tileset to find in resources/tilesetName.properties
     * @param loadImage   if false, won't load graphics (e.g. for the server, who needs info about the tileset, but we
     *                    don't want to load the images)
     * @throws TilesetNotFoundException
     */
    public Tileset(String tilesetName, boolean loadImage) throws TilesetNotFoundException {
        // load tileset configuration
//        File tilesetConf = new File();

        String image = null;
        this.nonWalkable = new ArrayList<>();

        Properties prop = new Properties();

        // read our config file
        try {
            InputStream input = GameAssets.getAssetStream(tilesetName + ".properties");
            prop.load(input);
            image = prop.getProperty("image");
            tileWidth = Integer.parseInt(prop.getProperty("width"));
            tileHeight = Integer.parseInt(prop.getProperty("height"));
            String[] nonWalkableTiles = prop.getProperty("nonwalkable").split(" ");
            for (String index : nonWalkableTiles) {
                nonWalkable.add(Integer.parseInt(index));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (image == null) {
            throw new TilesetNotFoundException();
        }
        if (loadImage) {
            sourceImage = new Image(image);
            cols = (int) sourceImage.getWidth() / tileWidth;
        }
    }

    public void drawTile(GraphicsContext ctx, int index, int x, int y) {
        int tileX = index % cols; // source X
        int tileY = index / cols; // source Y
        // draws the part of the tileset image that contains our tile
        ctx.drawImage(sourceImage, tileWidth * tileX, tileHeight * tileY, tileWidth, tileHeight,
                x * tileWidth, y * tileHeight, tileWidth, tileHeight);
    }

    public boolean isNonWalkable(Integer tileIndex) {
        return nonWalkable.contains(tileIndex);
    }

    // get, set
    public int getTileWidth() {
        return tileWidth;
    }

    public int getTileHeight() {
        return tileHeight;
    }
}
