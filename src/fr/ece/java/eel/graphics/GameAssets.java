package fr.ece.java.eel.graphics;

import fr.ece.java.eel.exception.AssetLoadingException;
import fr.ece.java.eel.exception.TilesetNotFoundException;
import javafx.scene.text.Font;

import java.io.InputStream;

/**
 * If we add more types of assets to the game (e.g. different tilesets, images, fonts, ...), we could add them and
 * handle their loading here
 */
public class GameAssets {
    Tileset tileset;

    Font smallerFont;

    public void load() throws AssetLoadingException {
        try {
            tileset = new Tileset();
        } catch (TilesetNotFoundException e) {
            throw new AssetLoadingException();
        }
    }

    public void serverLoad() throws AssetLoadingException {
        try {
            tileset = new Tileset(false);
        } catch (TilesetNotFoundException e) {
            throw new AssetLoadingException();
        }
    }

    public Tileset getTileset() {
        return tileset;
    }

    public Font getSmallerFont() {
        if (smallerFont == null) {
            smallerFont = new Font(10);
        }
        return smallerFont;
    }

    public static InputStream getAssetStream(String asset) {
        return ClassLoader.getSystemResourceAsStream(asset);
    }
}
