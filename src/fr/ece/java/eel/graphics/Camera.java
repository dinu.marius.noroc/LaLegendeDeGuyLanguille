package fr.ece.java.eel.graphics;

import fr.ece.java.eel.data.Player;
import fr.ece.java.eel.map.Map;
import javafx.scene.canvas.GraphicsContext;

public class Camera {
    private int camX;
    private int camY;
    private Player player;

    private int maxX;
    private int maxY;

    public Camera(Map m, Player p) {
        this.player = p;

        this.maxX = m.getWidth() * m.getTileset().getTileWidth();
        this.maxY = m.getHeight() * m.getTileset().getTileHeight();
    }

    public void updateCameraPosition(GraphicsContext ctx) {
        camX = player.getX() * player.getTileset().getTileWidth() - ((int) ctx.getCanvas().getWidth() / 2);
        camY = player.getY() * player.getTileset().getTileHeight() - ((int) ctx.getCanvas().getHeight() / 2);

        if (camX < 0) {
            camX = 0;
        } else if (camX >= maxX) {
            camX = maxX;
        }
        if (camY < 0) {
            camY = 0;
        } else if (camY >= maxY) {
            camY = maxY;
        }
    }

    public void applyCamera(GraphicsContext ctx) {
        ctx.translate(-camX, -camY);
    }
}
