package fr.ece.java.eel.data;

import fr.ece.java.eel.GameServerController;
import fr.ece.java.eel.Turn;
import fr.ece.java.eel.net.GameServerState;

import java.io.Serializable;

public class GameStatusUpdate extends ServerUpdate implements Serializable {
    private int playerCount;

    private GameServerState state;

    private Turn currentTurn;

    public GameStatusUpdate() {
        playerCount = 0;
        state = GameServerState.WAITING_PLAYERS;
        currentTurn = Turn.WAIT;
    }

    public GameStatusUpdate(GameServerController game) {
        this.playerCount = game.getPlayers().size();
        this.state = game.getNextGameState();
        this.currentTurn = game.getNextTurn();
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public GameStatusUpdate setPlayerCount(int playerCount) {
        this.playerCount = playerCount;
        return this;
    }

    public GameServerState getState() {
        return state;
    }

    public Turn getCurrentTurn() {
        return currentTurn;
    }
}
