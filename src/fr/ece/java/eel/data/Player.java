package fr.ece.java.eel.data;

import fr.ece.java.eel.action.Action;
import fr.ece.java.eel.Turn;
import fr.ece.java.eel.TurnManagerInterface;
import fr.ece.java.eel.exception.MaxQueueSizeException;
import fr.ece.java.eel.graphics.GameAssets;
import fr.ece.java.eel.map.Map;
import fr.ece.java.eel.net.PlayerThread;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class Player extends Entity {
    private static final int TILE_FOOT_PRINT = 7; // hardcoded for simplicity

    private Queue<Action> nextActions;

    private transient PlayerThread thread;

    private int maxMovementQueueSize = 4;

    private Turn turnNumber;

    private transient TurnManagerInterface turnManager;

    private int actionsThisTurn; // number of actions that have been done on that turn

    private int tileNumber;

    private Font font;

    public Player(Turn turnNumber, TurnManagerInterface turnManager) {
        super("<offline>");
        nextActions = new LinkedBlockingQueue<>();
        this.turnNumber = turnNumber;
        this.turnManager = turnManager;

        // hardcode the player tile number
        this.tileNumber = turnNumber.getNumber();
    }

    @Override
    public void load(GameAssets assets) {
        super.load(assets);
        this.font = assets.getSmallerFont();
    }


    @Override
    public void render(GraphicsContext ctx) {
        ctx.save();
        ctx.setTextAlign(TextAlignment.CENTER);
        ctx.setFont(font);

        // draw player name (centered)
        ctx.fillText(getName(), getX() * getTileset().getTileWidth() + getTileset().getTileWidth() / 2,
                getY() * getTileset().getTileHeight(), getTileset().getTileWidth());

        getTileset().drawTile(ctx, tileNumber, getX(), getY());

        ctx.restore();
    }

    // used by the game to draw foot prints on the next cells to go
    public void renderNextActions(GraphicsContext ctx, List<Action> actions) {
        int nextX = getX();
        int nextY = getY();
        int i = 0;
        for (Action action : actions) {
            // limit action preview to max queue size
            if (++i > maxMovementQueueSize) {
                break;
            }
            switch (action) {
                case RIGHT:
                    nextX++;
                    break;
                case LEFT:
                    nextX--;
                    break;
                case DOWN:
                    nextY++;
                    break;
                case UP:
                    nextY--;
                    break;
            }
            getTileset().drawTile(ctx, TILE_FOOT_PRINT, nextX, nextY);
        }
    }

    @Override
    public void update() {
        if (turnManager.getCurrentTurn().equals(turnNumber)) {
            // Turn management :
            // if the player isn't connected => skip turn
            // else => do actions, 1 per turn, max 4 actions. Wait until turn is complete
            if (thread == null) {
                turnManager.playerTurnDone(this);
                return;
            }
            // check if we have actions to do
            if (actionsThisTurn >= maxMovementQueueSize) {
                // we already did more actions than we can do, the turn is now done
                // this condition makes us wait until the player has done all it's actions before changing turn
                turnManager.playerTurnDone(this);
                return;
            }

            Action nextAction = nextActions.poll();
            if (nextAction != null) {
                switch (nextAction) {
                    case LEFT:
                    case RIGHT:
                    case UP:
                    case DOWN:
                        move(nextAction);
                        break;
                }
                actionsThisTurn++;
            }
        }
    }

    public void skipTurn() {
        turnManager.playerTurnDone(this);
        if (turnManager.getCurrentTurn().equals(turnNumber)) {
            actionsThisTurn = 0;
        }
    }

    public void notifyTurn() {
        actionsThisTurn = 0;
        if (thread != null) {
            thread.getServer().getGameController().playerChanged(this);
        }
    }

    public void notifyTurnEnd() {
        actionsThisTurn = 0;
        if (thread != null) {
            thread.getServer().getGameController().playerChanged(this);
        }
    }

    public void queueMove(Action action) throws MaxQueueSizeException {
        if (nextActions.size() >= maxMovementQueueSize || actionsThisTurn >= maxMovementQueueSize) {
            throw new MaxQueueSizeException();
        }
        nextActions.add(action);
    }

    public void cancelLastMove() {
        nextActions.poll();
    }

    /**
     * @return Is this player connected
     */
    public boolean isAvailable() {
        return thread == null;
    }

    public void assignThread(PlayerThread thread, String name) {
        this.thread = thread;
        setName(name);
    }

    public void removeThread() {
        this.thread = null;
        setName("<offline>");
    }

    @Override
    public String toString() {
        return "Player{" +
                getName() + " x=" + getX() + " y=" + getY() +
                " nextActions=" + nextActions + " turn=" + turnNumber +
                '}';
    }

    public Turn getTurnNumber() {
        return turnNumber;
    }

    /*
     * Two player are equal if they have the same turn number
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return turnNumber == player.turnNumber;

    }

    public Player spawnPoint(Map map) {
        int pos = map.getSpawn(turnNumber);
        setX(pos % map.getWidth());
        setY(pos / map.getWidth());
        return this;
    }

    @Override
    public int hashCode() {
        return turnNumber.hashCode();
    }

    public int getActionsThisTurn() {
        return actionsThisTurn;
    }

    public int getMaxMovementQueueSize() {
        return maxMovementQueueSize;
    }

    public Player setActionsThisTurn(int actionsThisTurn) {
        this.actionsThisTurn = actionsThisTurn;
        return this;
    }
}
