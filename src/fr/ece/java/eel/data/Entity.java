package fr.ece.java.eel.data;

import fr.ece.java.eel.action.Action;
import fr.ece.java.eel.graphics.GameAssets;
import fr.ece.java.eel.graphics.Tileset;
import fr.ece.java.eel.map.Map;
import javafx.scene.canvas.GraphicsContext;

import java.io.Serializable;

/**
 * Can represent any living thing on the map
 */
public abstract class Entity implements Serializable {
    // position
    private int x;
    private int y;

    // name
    private String name;

    // hp
    private int maxHP;
    private int currentHP;

    // tileset
    private Tileset tileset;

    // for server : indicates whether or not the update has been sent
    private transient boolean hasChanged;

    private transient Map map;
    private int remainingActions;

    public Entity(String name) {
        this.name = name;

        x = y = 0;
        currentHP = maxHP = -1;
    }

    public void load(GameAssets assets) {
        this.tileset = assets.getTileset();
    }

    public abstract void render(GraphicsContext ctx);

    public abstract void update();

    public void move(Action direction) {
        int oldX = x;
        int oldY = y;

        switch (direction) {
            case RIGHT:
                x++;
                if (x >= map.getWidth()) {
                    x = map.getWidth() - 1;
                }
                break;
            case LEFT:
                x--;
                if (x < 0) {
                    x = 0;
                }
                break;
            case UP:
                y--;
                if (y < 0) {
                    y = 0;
                }
                break;
            case DOWN:
                y++;
                if (y >= map.getHeight()) {
                    y = map.getHeight() - 1;
                }
                break;
        }

        if (!map.isWalkable(x, y)) {
            x = oldX;
            y = oldY;
        }

        hasChanged = true;
    }

    // getters, setters
    public int getX() {
        return x;
    }

    public Entity setX(int x) {
        this.x = x;
        hasChanged = true;
        return this;
    }

    public int getY() {
        return y;
    }

    public Entity setY(int y) {
        this.y = y;
        hasChanged = true;
        return this;
    }

    public String getName() {
        return name;
    }

    public Entity setName(String name) {
        this.name = name;
        return this;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public Entity setMaxHP(int maxHP) {
        this.maxHP = maxHP;
        return this;
    }

    public int getCurrentHP() {
        return currentHP;
    }

    public Entity setCurrentHP(int currentHP) {
        this.currentHP = currentHP > maxHP ? maxHP : currentHP;
        hasChanged = true;
        return this;
    }

    public Map getMap() {
        return map;
    }

    public Entity setMap(Map map) {
        this.map = map;
        return this;
    }

    public Tileset getTileset() {
        return tileset;
    }

    @Override
    public String toString() {
        return "Entity{" +
                "name='" + name + '\'' +
                '}';
    }

    public boolean hasChanged() {
        return hasChanged;
    }

    public void resetHasChanged() {
        hasChanged = false;
    }
}
