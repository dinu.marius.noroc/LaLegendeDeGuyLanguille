package fr.ece.java.eel.map;

import java.io.Serializable;
import java.util.*;

import fr.ece.java.eel.Turn;
import fr.ece.java.eel.data.Entity;
import fr.ece.java.eel.data.Player;
import fr.ece.java.eel.graphics.GameAssets;
import fr.ece.java.eel.graphics.Tileset;
import javafx.scene.canvas.GraphicsContext;

/**
 * Represents the game map
 * Contains it's own render method to render all the map elements
 */
public class Map implements Serializable {
    /**
     * Array that contains our cells:
     * map[layer][positionX + positionY * width] = a cell
     * <p>
     * A Cell object is basically an integer (the cell number to draw)
     */
    private Cell[][] map;

    /**
     * Contains our map entities (could be players, objects, monsters, ...)
     */
    private Set<Entity> entities;

    /**
     * Map size in tiles
     */
    private int width;
    private int height;

    /**
     * Player spawn list
     * E.G. PLAYER_1 => 143: the player 1 will spawn on cell number 143
     * This is used by the method getSpawn() that is called by the GameServerController on init
     */
    private java.util.Map<Turn, Integer> spawns;

    /**
     * The name of the map.
     * We originally wanted to dispay it in a fancy way
     */
    private String name;

    /**
     * The map tileset: the images we will cut in order use to draw the map
     */
    private transient Tileset tileset;

    public void load(GameAssets assets) {
        tileset = assets.getTileset();
    }

    /**
     * Creates a new Map object
     *
     * @param map    the cells data
     * @param height map height in tiles
     * @param width  map width in tiles
     */
    public Map(Cell[][] map, int height, int width) {
        spawns = new HashMap<>();
        entities = new LinkedHashSet<>();
        this.map = map;
        this.height = height;
        this.width = width;
    }

    /**
     * Draws the map and all it's entities
     */
    public void render(GraphicsContext ctx) {
        for (int layer = 0; layer < map.length; layer++) {
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    // AFFICHAGE DE LA MAP
                    // map[layer][x + width * y] => tile courante car on a enregristré nos tiles sur un array a une
                    // seule dimension plutot que faire un array 2D [y][x].
                    tileset.drawTile(ctx, map[layer][x + width * y].getIndex(), x, y);
                }
            }
        }
        for (Entity e : entities) {
            e.render(ctx);
        }
    }

    // ascii representation of the map (used for debug)
    public void renderAscii() {
        // array containing the map we will display (we need to merge all the layers)
        char[] asciiMap = new char[width * height];
        Cell temp;
        // layers
        for (int layer = 0; layer <= map.length - 1; layer++) {
            // cells
            for (int cell = 0; cell <= width * height - 1; cell++) {
                // put 0 if cell empty, or it's ASCII representation otherwise
                if (layer == 0) {
                    asciiMap[cell] = '0';
                }
                temp = map[layer][cell];
                if (temp.getIndex() > 0) {
                    asciiMap[cell] = (char) ('0' + temp.getIndex());
                }
            }
        }
        // add entities into the array
        for (Entity e : entities) {
            asciiMap[e.getX() + width * e.getY()] = e.getName().charAt(0);
            System.out.println(e);
        }
        // actual display
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                System.out.print(asciiMap[x + width * y]);
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    /**
     * Adds an entity to the map, also sets the entity's map field to this
     */
    public void addEntity(Entity e) {
        e.setMap(this);
        this.entities.add(e);
    }

    public void removeEntity(Player player) {
        this.entities.remove(player);
    }

    /**
     * Detect if the cell x, y is walkable
     * E.g. you cant walk on water. But if there is a walkable cell above the water (i.e. a bridge), you can walk on it.
     * Also you can't walk on the void.
     *
     * Void is a special case because it can be found on a higher layer (e.g. above water) but that doesn't mean we can
     * walk on that water.
     *
     * So, the walkability is the walkability of the highest non-void cell
     */
    public boolean isWalkable(int x, int y) {
        boolean isWalkable = false;
        int index;
        // for each layer
        for (int layer = 0; layer < map.length; layer++) {
            index = map[layer][x + width * y].getIndex();
            if (index == 0) {
                // it's void, we ignore this
                continue;
            }
            // it's not void
            isWalkable = !tileset.isNonWalkable(index);
        }
        return isWalkable;
    }

    /**
     * Return spawn point for player X (1, 2, 3, 4)
     *
     * @param player e.g. PLAYER_1
     * @return spawn coordinate
     */
    public int getSpawn(Turn player) {
        return spawns.getOrDefault(player, 0);
    }

    // Mindless getters, setters

    void setSpawn(Turn player, int spawn) {
        spawns.put(player, spawn);
    }

    public Set<Entity> getEntities() {
        return entities;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Tileset getTileset() {
        return tileset;
    }

}
