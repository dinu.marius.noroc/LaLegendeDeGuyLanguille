package fr.ece.java.eel.map;

import fr.ece.java.eel.Turn;
import fr.ece.java.eel.exception.MapNotFoundException;
import fr.ece.java.eel.graphics.GameAssets;

import java.io.*;
import java.util.*;

/**
 * Reads a map from a set of files
 */
public class MapReader {
    /**
     * Reads a map from the resources/maps/ directory
     * A map is represented by a folder containing:
     * - map.properties file: general information about the map
     * - x.csv: where x represents a layer, it is the list of cells that are on the layer
     *
     * @param levelName e.g. level1
     */
    public Map loadLevel(String levelName) throws MapNotFoundException {
        String mapDir = "maps/" + levelName + "/";

        int layerCount, width, height;
        Properties prop = new Properties();
        try {
            // load map : load our map.properties file
            InputStream input = GameAssets.getAssetStream(mapDir + "map.properties");
            prop.load(input); // read properties
            layerCount = Integer.parseInt(prop.getProperty("layers"));
            width = Integer.parseInt(prop.getProperty("width"));
            height = Integer.parseInt(prop.getProperty("height"));

            // init our grid : read each .csv describing the map in the folder. layer 0 : 0.csv, layer 1 : 1.csv, ...
            Cell[][] map = new Cell[layerCount][];
            for (int i = 0; i <= layerCount - 1; i++) {
                map[i] = readLayer(mapDir + i + ".csv", height * width);
            }

            // init our map object
            Map resultMap = new Map(map, height, width);
            resultMap.setName(prop.getProperty("name"));
            resultMap.setSpawn(Turn.PLAYER_1, Integer.parseInt(prop.getProperty("spawn1")));
            resultMap.setSpawn(Turn.PLAYER_2, Integer.parseInt(prop.getProperty("spawn2")));
            resultMap.setSpawn(Turn.PLAYER_3, Integer.parseInt(prop.getProperty("spawn3")));
            resultMap.setSpawn(Turn.PLAYER_4, Integer.parseInt(prop.getProperty("spawn4")));

            return resultMap;
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new MapNotFoundException();
        }
    }

    /**
     * Reads a map layer, e.g. an individual X.csv file
     */
    private Cell[] readLayer(String fileName, int size) throws IOException {
        // read the csv : comma separated tile numbers
        BufferedReader input = new BufferedReader(new InputStreamReader(GameAssets.getAssetStream(fileName)));
        Cell[] layer = new Cell[size];

        int i = 0;
        String line;
        while ((line = input.readLine()) != null) {
            // csv line = map row
            String[] cells = line.split(",");
            for (String cellNum : cells) {
                // read every index
                layer[i] = new Cell(Integer.parseInt(cellNum));
                if (i++ >= layer.length) {
                    break;
                }
            }
        }

        return layer;
    }

    // test map loading
    public static void main(String[] args) {
        MapReader mr = new MapReader();
        try {
            Map m = mr.loadLevel("level1");
            if (m != null) {
                m.renderAscii();
            } else {
                System.err.println("nullmap");
            }
        } catch (MapNotFoundException e) {
            e.printStackTrace();
        }
    }
}