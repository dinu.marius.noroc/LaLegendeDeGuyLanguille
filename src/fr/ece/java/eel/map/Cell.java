package fr.ece.java.eel.map;

import java.io.Serializable;

/**
 * Represents a cell
 * We could have used an integer instead, but we originally planned to do more here.
 */
public class Cell implements Serializable {
    private int index;

    public Cell(int index) {
        this.index = index < 0 ? 0 : index;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return "Cell " + index;
    }
}
