package fr.ece.java.eel;

public enum Turn {
    WAIT(-1),
    PLAYER_1(1),
    PLAYER_2(2),
    PLAYER_3(3),
    PLAYER_4(4),
    AI(5);

    private final int number;

    Turn(int number) {
        this.number = number;
    }

    public static Turn fromNumber(int number) {
        for (Turn turn : Turn.values()) {
            if (number == turn.number) {
                return turn;
            }
        }
        return null;
    }

    public int getNumber() {
        return number;
    }

    public Turn nextTurn() {
        if (equals(WAIT)) {
            return PLAYER_1;
        } else if (number <= 4) { // turns 1, 2, 3, 4, AI
            return fromNumber(number + 1);
        } else {
            return PLAYER_1;
        }
    }
}
