package fr.ece.java.eel.action;

public enum Action {
    NONE,
    UP,
    RIGHT,
    DOWN,
    LEFT,
    ATTACK
}
