package fr.ece.java.eel;

import fr.ece.java.eel.data.Player;

public interface TurnManagerInterface {
    void playerTurnDone(Player player);

    Turn getCurrentTurn();
}
