package fr.ece.java.eel.client;

import fr.ece.java.eel.Turn;
import fr.ece.java.eel.data.Entity;
import fr.ece.java.eel.data.Player;
import fr.ece.java.eel.data.PlayerUpdate;
import fr.ece.java.eel.data.GameStatusUpdate;
import fr.ece.java.eel.exception.AssetLoadingException;
import fr.ece.java.eel.map.Map;
import fr.ece.java.eel.net.protocol.Command;
import fr.ece.java.eel.net.protocol.GameProtocol;
import javafx.scene.canvas.GraphicsContext;

import java.io.*;
import java.net.ConnectException;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

public class Client extends Thread {
    private Socket socket;

    private GameProtocol protocol;

    private String name;

    private String host;
    private int port;

    private GameClient game;

    private App app;

    public Client(App app, String host, int port, String playerName) {
        this.host = host;
        this.port = port;
        this.name = playerName;
        this.app = app;
    }

    /**
     * Thread started!
     * Let's connect to the server and start our client life loop: read until we disconnect.
     */
    public void run() {
        try {
            // connect
            System.out.println("Connexion a " + host + ":" + port);
            socket = new Socket(host, port);
            System.out.printf("Connecté.");

            // setup streams
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            // setup GameProtocol
            protocol = new GameProtocol(out, in);

            // send our player name to the server
            protocol.sendName(name);

            // inits
            GameStatusUpdate status = null;
            Map map = null;
            boolean initCommandsSent = false;
            Turn playerTurnNumber = null;

            // read from socket
            while (!socket.isClosed()) {
                String line = in.readLine();
                if (line == null) {
                    // connection closed
                    break;
                }

                // display socket output (debug)
//                System.out.println(line);

                // Handle input, checkReceivedLine returns an object if it receives a serialized object from the socket
                // e.g. if we pass it line = "<<<OBJECT", checkReceivedLine() will read the next line in the socket in
                // order to deserialize the received object.
                Object o;
                if ((o = protocol.checkReceivedLine(line)) != null) {
                    if (o instanceof Map) {
                        System.out.println("-> Received map");
                        map = (Map) o;
                    } else if (o instanceof GameStatusUpdate) {
                        System.out.println("-> Received server status");
                        status = (GameStatusUpdate) o;

                        if (game != null) {
                            game.handleStatusUpdate(status);
                        }
                    } else if (o instanceof PlayerUpdate) {
                        System.out.println("-> Received player update");
                        protocol.handlePlayerUpdate(game, (PlayerUpdate) o);
                    } else if (o instanceof Turn) {
                        System.out.println("-> Received turn number");
                        // we received our turn number
                        playerTurnNumber = (Turn) o;
                    }
                }

                // if we have not already sent the commands for our init sequence
                if (!initCommandsSent) {
                    protocol.sendCommand(Command.MAP);
                    initCommandsSent = true;
                }

                // if the game is not yet initialized
                if (game == null && map != null && playerTurnNumber != null) {
                    Set<Player> playerSet = new HashSet<Player>();
                    // build our player list
                    for (Entity e : map.getEntities()) {
                        if (e instanceof Player) {
                            playerSet.add((Player) e);
                        }
                    }

                    // create a client game controller
                    game = new GameClient(playerSet, map, playerTurnNumber, protocol);
                    app.gameInitialized(game); // bind keys

                    // ask for a status update
                    protocol.sendCommand(Command.STATUS);
                }
            }
            app.gameQuit();
        } catch (ConnectException e) {
            // connection refused
            app.showError("Le serveur est plein ou injoignable.");
        } catch (IOException e) {
            // can happen if the server disconnects
            app.showError("Une erreur de communication est survenue.");
            e.printStackTrace();
        } catch (AssetLoadingException e) {
            app.showError("Impossible de charger les ressources du jeu. Assurez vous que le dossier resources est " +
                    "présent et enregistré dans le classpath.");
        }
    }

    /**
     * The game render
     */
    public void renderGame(GraphicsContext ctx) {
        if (game == null) {
            // game isn't initialized yet
            ctx.fillText("Connexion et chargement...", 10, 40);
        } else {
            game.render(ctx);
        }
    }

    /**
     * Client shutdown hook: properly close our connection.
     */
    public void shutdown() {
        if (socket == null) {
            return;
        }
        try {
            socket.close();
        } catch (IOException e) {
            // couldn't close the socket, probably because it was already closed (e.g. server side disconnect)
        }
    }
}
