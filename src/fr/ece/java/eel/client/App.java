package fr.ece.java.eel.client;

import fr.ece.java.eel.Main;
import fr.ece.java.eel.net.Server;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.Map;
import java.util.Optional;

public class App extends Application {
    /**
     * Handles the game client: connects to the server and starts a GameClient instance
     */
    Client client;

    Scene scene;

    @Override
    public void start(Stage primaryStage) {
        // Get CLI params
        int port;
        String host;

        Map<String, String> params = getParameters().getNamed();
        port = Integer.parseInt(params.getOrDefault("port", String.valueOf(Server.DEFAULT_PORT)));
        host = params.getOrDefault("host", "localhost");

        // Window configuration
        primaryStage.setTitle("La Légende de Guy l'Anguille");

        Group group = new Group();
        scene = new Scene(group);
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.setWidth(800);
        primaryStage.setHeight(600);

        // Create our canvas + our canvas holder
        StackPane pane = new StackPane();
        pane.setStyle("-fx-background-color: black");
        Canvas canvas = new Canvas(800, 600);
        pane.getChildren().add(canvas);
        group.getChildren().add(pane);

        // Set default app font & get our graphicsContext
        Font font = Font.font(16);
        GraphicsContext ctx = canvas.getGraphicsContext2D();
        ctx.setFont(font);

        // Ask the player for it's name
        String name = askName();
        if (name == null) {
            // user cancelled
            gameQuit();
            return;
        }

        // Start the client thread
        client = new Client(this, host, port, name);
        client.setDaemon(true);
        client.start();

        // JavaFX provides us a timer that is called every time we have to redraw on the canvas
        new AnimationTimer() {
            @Override
            public void handle(long now) {
                // reset colors
                ctx.setFill(Color.WHITE);
                ctx.setStroke(Color.WHITE);

                // clear canvas
                ctx.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

                // render game
                client.renderGame(ctx);
            }
        }.start();

        // properly close the socket on app shutdown
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                client.shutdown();
            }
        });

        // Finally, when everything is ready, display the app !
        primaryStage.show();
    }

    /**
     * Callback called by the client when the game is initialized
     * This allows us to give to the game the responsibility of handling user input
     */
    public void gameInitialized(GameClient game) {
        // when the game controller is initialised, we start passing it the user input
        scene.setOnKeyReleased(e -> game.handleKeyReleased(e.getCode()));
    }

    /**
     * Exits the app.
     * Platform.runLater() allows us to run the operation on the JavaFX UI thread
     */
    public void gameQuit() {
        Platform.runLater(Platform::exit);
    }

    /**
     * Displays an error box
     */
    public void showError(String error) {
        // we need to run this from the UI thread (handled by Java FX)
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur d'application");
            alert.setHeaderText("Oops, une erreur s'est produite !");
            alert.setContentText(error);
            alert.showAndWait();
            Platform.exit();
        });
    }

    private String askName() {
        TextInputDialog dialog = new TextInputDialog("Guy");
        dialog.setTitle("La légende de Guy L'anguile");
        dialog.setHeaderText("Entrez votre nom");
        dialog.setContentText("Entrez votre nom :");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            return result.get();
        } else {
            return null;
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

}
