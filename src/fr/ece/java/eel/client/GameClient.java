package fr.ece.java.eel.client;

import fr.ece.java.eel.action.Action;
import fr.ece.java.eel.Turn;
import fr.ece.java.eel.data.GameStatusUpdate;
import fr.ece.java.eel.data.Player;
import fr.ece.java.eel.data.PlayerUpdate;
import fr.ece.java.eel.exception.AssetLoadingException;
import fr.ece.java.eel.graphics.Camera;
import fr.ece.java.eel.graphics.GameAssets;
import fr.ece.java.eel.map.Map;
import fr.ece.java.eel.net.GameServerState;
import fr.ece.java.eel.net.protocol.Command;
import fr.ece.java.eel.net.protocol.GameProtocol;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;

import java.util.*;

public class GameClient {
    private final GameProtocol protocol;
    private Map map;
    private java.util.Map<Turn, Player> players;
    private Turn currentTurn;
    private GameAssets assets;
    private GameServerState gameState;

    private List<Action> nextActions; // see Player::renderNextActions()

    private Player currentPlayer;

    private Camera camera;

    // client init
    public GameClient(Set<Player> players, Map map, Turn ourPlayerTurn, GameProtocol protocol) throws AssetLoadingException {
        // build the (turn, player) map
        this.players = new LinkedHashMap<>();
        for (Player player : players) {
            this.players.put(player.getTurnNumber(), player);
        }
        this.currentPlayer = this.players.get(ourPlayerTurn);

        this.assets = new GameAssets();
        this.map = map;

        // init asset loader
        assets.load();

        // init map assets
        map.load(assets);

        // init players resources
        this.players.forEach((turn, player) -> {
            player.load(assets);
        });

        currentTurn = Turn.WAIT;

        this.protocol = protocol;

        camera = new Camera(map, currentPlayer);
        nextActions = new ArrayList<>();
    }

    public void render(GraphicsContext ctx) {
        camera.updateCameraPosition(ctx);

        ctx.save(); // save the untranslated context
        camera.applyCamera(ctx);
        map.render(ctx); // render tiles with camera translation
        currentPlayer.renderNextActions(ctx, nextActions); // footprints
        ctx.restore(); // restore to the original translation

        ctx.fillText("C'est le tour de " + currentTurn.toString(), 10, 40);
        ctx.fillText("Vous êtes le joueur " + currentPlayer.getTurnNumber().getNumber() + " : "
                + currentPlayer.getName() + " !", 10, 70);

        if (currentTurn.equals(currentPlayer.getTurnNumber())) {
            int remaining = currentPlayer.getMaxMovementQueueSize() - nextActions.size()
                    - currentPlayer.getActionsThisTurn();
            ctx.fillText("Il vous reste " + (remaining > 0 ? remaining : 0) + " actions à faire.", 10, 90);
        }
    }

    public void handlePlayerUpdate(PlayerUpdate update) {
        players.get(update.getTurnNumber())
                .setActionsThisTurn(update.getActionsThisTurn())
                .setX(update.getX())
                .setY(update.getY())
                .setName(update.getName());
        if (update.getTurnNumber().equals(currentPlayer.getTurnNumber())) {
            nextActions.clear();
        }
    }

    public void handleStatusUpdate(GameStatusUpdate status) {
        gameState = status.getState();
        currentTurn = status.getCurrentTurn();
    }

    public java.util.Map<Turn, Player> getPlayers() {
        return players;
    }

    public void handleKeyReleased(KeyCode code) {
        switch (code) {
            case LEFT:
                protocol.sendPlayerMove(Action.LEFT);
                nextActions.add(Action.LEFT);
                break;
            case RIGHT:
                protocol.sendPlayerMove(Action.RIGHT);
                nextActions.add(Action.RIGHT);
                break;
            case DOWN:
                protocol.sendPlayerMove(Action.DOWN);
                nextActions.add(Action.DOWN);
                break;
            case UP:
                protocol.sendPlayerMove(Action.UP);
                nextActions.add(Action.UP);
                break;
            case ENTER:
                protocol.sendCommand(Command.DONE);
                nextActions.clear();
                break;
        }
    }
}
